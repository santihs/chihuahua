'use client';

import {
  Tab,
  TabGroup,
  TabList,
  TabPanel,
  TabPanels,
  Grid,
  Card,
} from '@tremor/react';
import KpiCardGrid from '@/components/KpiCard';
import ChartView from '@/components/ChartView';
import BarView from '@/components/BarView';
import axios from 'axios';

const PATH = process.env.BACKEND;

async function loadData() {
  const responseCat = await axios.get(
    `${PATH ?? 'http://localhost:5001'}/history/pet/cat`,
  );
  const responseDog = await axios.get(
    `${PATH ?? 'http://localhost:5001'}/history/pet/dog`,
  );

  const totalPet: number = responseDog.data.length + responseCat.data.length;
  const totalCat: number = responseCat.data.length;
  const totalDog: number = responseDog.data.length;
  return {
    quantityDog: totalCat,
    quantityCat: totalDog,
    total: totalPet,
  };
}

export async function Dashboard() {
  const data = await loadData();
  const kpiCardsList = [
    {
      petName: 'Dog',
      quantity: data.quantityDog,
      percentage: (data.quantityDog * 100) / data.total,
      total: data.total,
    },
    {
      petName: 'Cat',
      quantity: data.quantityCat,
      percentage: (data.quantityCat * 100) / data.total,
      total: data.total,
    },
  ];
  return (
    <>
      <TabGroup className="mt-6">
        <TabList>
          <Tab>Overview</Tab>
          <Tab>Detail</Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <Grid numItemsMd={2} numItemsLg={2} className="gap-6 mt-6">
              {kpiCardsList.map((pet, index) => (
                <Card key={index}>
                  <KpiCardGrid
                    petName={pet.petName}
                    quantity={pet.quantity}
                    percentage={pet.percentage}
                    total={pet.total}
                  />
                </Card>
              ))}
            </Grid>
            <div className="mt-6">
              <Card>
                <ChartView />
              </Card>
            </div>
          </TabPanel>
          <TabPanel>
            <div className="mt-6">
              <Card>
                <BarView />
              </Card>
            </div>
          </TabPanel>
        </TabPanels>
      </TabGroup>
    </>
  );
}
