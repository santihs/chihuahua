import {
  Card,
  CardBody,
  CardFooter,
  Heading,
  Button,
  Stack,
  Image,
  Divider,
  ButtonGroup,
  Link,
} from '@chakra-ui/react';
import CustomWebcam from '../components/CustomWebcam';
import Layout from '../components/Layout';
import React from 'react';

function CardItem() {
  return (
    <>
      <Layout />
      <Heading fontSize="5xl">List Camaras</Heading>
      <Card maxW="sm">
        <CardBody>
          <Image
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Cuesta_del_obispo_01.jpg/800px-Cuesta_del_obispo_01.jpg"
            alt="image"
            borderRadius="lg"
          />
          <Stack mt="6" spacing="3">
            <Heading size="md">User 1</Heading>
          </Stack>
        </CardBody>
        <Divider />
        <CardFooter>
          <ButtonGroup spacing="2">
            <Button variant="solid" colorScheme="blue">
              <Link href="/CardWebcam">View</Link>
            </Button>
          </ButtonGroup>
        </CardFooter>
      </Card>
      <CustomWebcam show={'none'} />
    </>
  );
}

export default CardItem;
