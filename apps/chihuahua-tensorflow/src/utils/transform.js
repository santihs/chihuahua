export const transform = (base64) => {
  const base64Data = base64.replace(/^data:image\/\w+;base64,/, '');

  const imageBuffer = Buffer.from(base64Data, 'base64');
  return imageBuffer;
};
