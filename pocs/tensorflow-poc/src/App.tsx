import Webcam from 'react-webcam'
import './App.css'
import '@tensorflow/tfjs'
import useCameraRecognition from './hooks/useCameraRecognition'

function App() {

  const { webcamRef, canvasRef } = useCameraRecognition()

  return (
    <>
      <Webcam
        ref={webcamRef}
        muted={true}
      />

      <canvas
        ref={canvasRef}

        style={{
          position: "absolute",
          marginLeft: "auto",
          marginRight: "auto",
          left: 0,
          right: 0,
          textAlign: "center",
          width: 640,
          height: 480,
        }}
      />
    </>
  )
}

export default App
