import Link from 'next/link';
import Layout from '../components/Layout';
import CustomWebcam from '../components/CustomWebcam';
import { Heading, Image } from '@chakra-ui/react';
import React from 'react';

const IndexPage = () => (
  <Layout title="Home | Next.js + TypeScript Example">
    <Heading fontSize="5xl">Hello User 👋</Heading>

    <Link href="/about">About</Link>
    <br />
    <Link href="/CardItem">Cards</Link>

    <CustomWebcam show={'none'} />
    <Image
      src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Cuesta_del_obispo_01.jpg/800px-Cuesta_del_obispo_01.jpg"
      alt="image"
      borderRadius="lg"
    />
  </Layout>
);

export default IndexPage;
