'use client';

import {
  AtSignIcon,
  InfoOutlineIcon,
  RepeatClockIcon,
  ViewIcon,
} from '@chakra-ui/icons';
import {
  Box,
  useColorModeValue,
  Drawer,
  DrawerContent,
  useDisclosure,
  LinkBox,
  Stack,
  Center,
  Heading,
} from '@chakra-ui/react';
import { SidebarButton } from '@/components/SideBarButton';
import SessionProviderWrapper from '@/utils/sessionProviderWraper';
import AuthStatus from '@/components/AuthStatus';

export default function SimpleSidebar() {
  const { isOpen, onClose } = useDisclosure();

  return (
    <Box minH="100vh" bg={useColorModeValue('gray.100', 'gray.400')}>
      <SidebarContent />
      <Drawer
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        returnFocusOnClose={false}
        onOverlayClick={onClose}
        size="full"
      >
        <DrawerContent>
          <SidebarContent />
        </DrawerContent>
      </Drawer>
    </Box>
  );
}

const SidebarContent = () => {
  return (
    <Box
      bg={useColorModeValue('white', 'gray.900')}
      borderRight="1px"
      borderRightColor={useColorModeValue('gray.200', 'gray.700')}
      width={{ base: 'full', md: 80 }}
      pos="fixed"
      height="full"
    >
      <LinkBox as="article" maxW="sm" p="5">
        <Center>
          <Box
            as="a"
            color="black"
            href="/"
            fontWeight="bold"
            fontSize="4xl"
            mt="3"
            _hover={{ bg: 'white', textColor: 'secondary' }}
          >
            <Heading>Pet Patrol</Heading>
          </Box>
        </Center>
      </LinkBox>
      <Stack spacing={2} direction="column" align="center">
        <SidebarButton icon={ViewIcon} label={'Camera'} route={'/camera'} />
        <SidebarButton
          icon={InfoOutlineIcon}
          label={'Dashboard'}
          route={'/dashboard'}
        />
        <SidebarButton
          icon={RepeatClockIcon}
          label={'History'}
          route={'/history'}
        />
        <SidebarButton icon={AtSignIcon} label={'Profile'} route={'/profile'} />
        <SessionProviderWrapper>
          <AuthStatus />
        </SessionProviderWrapper>
      </Stack>
    </Box>
  );
};
