import React from 'react';
import Webcam from 'react-webcam';

const CustomWebcam = ({ show }) => {
  const webcamRef = React.useRef(null);

  return <Webcam ref={webcamRef} style={{ display: show }} />;
};

export default CustomWebcam;
