import fastifyPlugin from 'fastify-plugin';
import fastifyMongo from '@fastify/mongodb';
import { FastifyPluginOptions } from 'fastify';

async function mongoConnector(fastify: FastifyPluginOptions) {
  fastify.register(fastifyMongo, {
    url: 'mongodb://admin:admin@localhost:27017',
    database: 'mydatabase',
  });
}

export default fastifyPlugin(mongoConnector);
