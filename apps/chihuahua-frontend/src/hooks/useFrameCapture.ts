import { useEffect, useRef } from 'react';
import Webcam from 'react-webcam';
import socketIO from 'socket.io-client';

const useFrameCapture = () => {
  const webcamRef = useRef<Webcam>(null);
  const canvasRef = useRef<HTMLCanvasElement>(null);

  const captureFrame = () => {
    if (
      typeof webcamRef.current !== 'undefined' &&
      webcamRef.current !== null &&
      webcamRef.current?.video?.readyState === 4
    ) {
      const video = webcamRef.current.video;
      const videoWidth = webcamRef.current?.video.videoWidth;
      const videoHeight = webcamRef.current?.video.videoHeight;

      webcamRef.current.video.width = videoWidth;
      webcamRef.current.video.height = videoHeight;

      if (canvasRef.current !== null) {
        canvasRef.current.width = videoWidth;
        canvasRef.current.height = videoHeight;
      }

      const ctx = canvasRef.current?.getContext('2d');

      ctx?.drawImage(video, 0, 0, video.width, video.height);

      const base64 = canvasRef.current?.toDataURL('image/jpeg');
      return base64;
    }
  };

  useEffect(() => {
    const interval = setInterval(() => {
      const socket = socketIO('http://localhost:9090');

      const base64 = captureFrame();
      socket.emit('message', base64);

      return () => {
        socket.close();
      };
    }, 5000);
    return () => {
      clearInterval(interval);
    };
  }, []);
  return { webcamRef, canvasRef };
};

export default useFrameCapture;
