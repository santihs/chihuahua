import { Resend } from 'resend';
import dotenv from 'dotenv';
dotenv.config();

const Vfrom = 'Admin <onboarding@resend.dev>';
const Vto = ['jmacedocallejas2002@gmail.com'];
const Vsubject = 'new test 3';
const Vhtml = '<strong>It works!</strong>';

async function sendEmail() {
  const resend = new Resend(process.env.RESEND_API_KEY);

  try {
    const data = await resend.emails.send({
      from: Vfrom,
      to: Vto,
      subject: Vsubject,
      html: Vhtml,
    });

    console.log(data);
  } catch (error) {
    console.error(error);
  }
}

sendEmail();
