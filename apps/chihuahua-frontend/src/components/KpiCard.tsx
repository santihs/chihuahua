'use client';

import { Card, Flex, Metric, ProgressBar, Text } from '@tremor/react';

type Props = {
  petName: string;
  quantity: number;
  percentage: number;
  total: number;
};

function KpiCard({ petName, quantity, percentage, total }: Props) {
  return (
    <Card className="max-w-lg mx-auto">
      <Flex alignItems="start">
        <div>
          <Text>{petName}</Text>
          <Metric>{quantity}</Metric>
        </div>
      </Flex>
      <Flex className="mt-4">
        <Text className="truncate">
          {percentage}% ({quantity})
        </Text>
        <Text>{total}</Text>
      </Flex>
      <ProgressBar value={percentage} className="mt-2" />
    </Card>
  );
}

export default KpiCard;
