import { BarList, Bold, Card, Flex, Text, Title } from '@tremor/react';
import axios from 'axios';

const PATH = process.env.BACKEND;

async function loadData() {
  const responseCat = await axios.get(
    `${PATH ?? 'http://localhost:5001'}/history/pet/cat`,
  );
  const responseDog = await axios.get(
    `${PATH ?? 'http://localhost:5001'}/history/pet/dog`,
  );

  const totalPet: number = responseDog.data.length + responseCat.data.length;
  const totalCat: number = responseCat.data.length;
  const totalDog: number = responseDog.data.length;
  return {
    quantityDog: totalCat,
    quantityCat: totalDog,
    total: totalPet,
  };
}

export default async function BarView() {
  const dataPet = await loadData();
  const data = [
    {
      name: 'Dogs',
      value: dataPet.quantityDog,
    },
    {
      name: 'Cats',
      value: dataPet.quantityCat,
    },
  ];
  return (
    <Card className="max-w-lg">
      <Title>Pet Analytics</Title>
      <Flex className="mt-4">
        <Text>
          <Bold>Pet</Bold>
        </Text>
        <Text>
          <Bold>Total</Bold>
        </Text>
      </Flex>
      <BarList data={data} className="mt-2" />
    </Card>
  );
}
