import amqp from 'amqplib';

const rabbitMqUrl =
  'amqps://acaumfcb:WQg94XetSnqvGzF_RHmHf1m6cptVfzCh@fly.rmq.cloudamqp.com/acaumfcb';
const queue = 'prediciton-queue';

export const publishPrediction = async (prediction) => {
  const connection = await amqp.connect(rabbitMqUrl);

  const channel = await connection.createChannel();
  await channel.assertQueue(queue, { durable: false });
  channel.sendToQueue(queue, Buffer.from(JSON.stringify(prediction)));

  console.info(`Sent ${JSON.stringify(prediction)} to queue ${queue}`);

  await channel.close();
  await connection.close();
};
