'use client';

import SimpleSidebar from '@/components/SideBar';
import {
  Avatar,
  Box,
  Center,
  Grid,
  GridItem,
  Heading,
  useColorModeValue,
  Text,
} from '@chakra-ui/react';
import { useSession } from 'next-auth/react';

const useUserBackground = () => {
  const colorMode = useColorModeValue('white', 'gray.900');
  return colorMode === 'white' ? 'white' : 'gray.900';
};

export default function Profile() {
  const { data: session, status } = useSession({
    required: true,
  });
  const userBackground = useUserBackground();

  if (status === 'loading') {
    return 'Loading or not authenticated...';
  }

  return (
    <>
      <Grid
        templateAreas={`
                  "nav main"`}
        gridTemplateRows={'50px 1fr 30px'}
        gridTemplateColumns={'150px 1fr'}
        gap="1"
        fontWeight="bold"
      >
        <GridItem pl="0" area={'nav'}>
          <SimpleSidebar />
        </GridItem>
        <GridItem ml="200" area={'main'} mr="45">
          <Heading className="text-center" fontSize="6xl" mt="40px" mb="45">
            Profile
          </Heading>
          <Center py={6}>
            <Box
              maxW={'720px'}
              w={'full'}
              bg={userBackground}
              boxShadow={'2xl'}
              rounded={'lg'}
              p={10}
              textAlign={'center'}
            >
              <Avatar
                size="2xl"
                src={
                  'https://static-00.iconduck.com/assets.00/profile-default-icon-2048x2045-u3j7s5nj.png'
                }
                mb={4}
                pos={'relative'}
              />
              <Box textAlign={'left'}>
                <Text fontSize={'xl'} fontFamily={'body'}>
                  Name:
                </Text>
              </Box>
              <Text fontSize={'2xl'} fontFamily={'body'} mb="5">
                {session?.user?.name}
              </Text>
              <Box textAlign={'left'}>
                <Text fontSize={'xl'} fontFamily={'body'}>
                  Email:
                </Text>
              </Box>
              <Text fontSize={'2xl'} fontFamily={'body'} mb="5">
                {session?.user?.email}
              </Text>
              <Box textAlign={'left'}>
                <Text fontSize={'xl'} fontFamily={'body'}>
                  Role:
                </Text>
              </Box>
              <Text fontSize={'2xl'} fontFamily={'body'}>
                Veterinary
              </Text>
            </Box>
          </Center>
        </GridItem>
      </Grid>
    </>
  );
}
