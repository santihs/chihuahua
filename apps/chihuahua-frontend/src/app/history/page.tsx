'use client';

import SimpleSidebar from '@/components/SideBar';
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
  Grid,
  GridItem,
  Heading,
  RadioGroup,
  Stack,
  Radio,
} from '@chakra-ui/react';
import { ChangeEvent, useEffect, useState } from 'react';
import { historyApi } from '../api/history/HistoryApi';
import { useSession } from 'next-auth/react';
import { ObjectId } from '@fastify/mongodb';

interface historyItem {
  _id: ObjectId;
  pet: string;
  score: number;
  place: string;
  day: string;
  time: string;
}

function History() {
  const [history, setHistory] = useState<historyItem[]>([]);
  const [value, setValue] = useState('');

  const { status } = useSession({
    required: true,
  });

  useEffect(() => {
    const fetchStatus = async () => {
      try {
        const statusData = await historyApi.getAllStatus();
        setHistory(statusData);
      } catch (error) {
        console.error('Error fetching status:', error);
      }
    };

    if (status === 'loading') {
      return;
    }

    fetchStatus();
  }, [status]);

  const fetchStatusPet = (
    event: ChangeEvent<HTMLInputElement>,
    pet: string,
  ) => {
    historyApi
      .getAllPets(pet)
      .then((statusData) => {
        setHistory(statusData);
      })
      .catch((error) => {
        console.error('Error fetching status:', error);
      });
  };

  const fetchGetAll = (event: ChangeEvent<HTMLInputElement>) => {
    historyApi
      .getAllStatus()
      .then((statusData) => {
        setHistory(statusData);
      })
      .catch((error) => {
        console.error('Error fetching status:', error);
      });
  };

  return (
    <>
      <Grid
        templateAreas={`
                  "nav main"
                  `}
        gridTemplateRows={'50px 1fr 30px'}
        gridTemplateColumns={'150px 1fr'}
        gap="1"
      >
        <GridItem area={'nav'}>
          <SimpleSidebar />
        </GridItem>
        <GridItem ml="200" mr="45" area={'main'}>
          <Heading className="text-center" fontSize="6xl" mt="10px" mb="20px">
            History
          </Heading>
          <RadioGroup onChange={setValue} value={value}>
            <Stack direction="row" spacing={10} ml="35">
              <Radio
                value="dog"
                size="lg"
                onChange={(e) => {
                  fetchStatusPet(e, 'dog');
                }}
              >
                Dog
              </Radio>
              <Radio
                value="cat"
                size="lg"
                onChange={(e) => {
                  fetchStatusPet(e, 'cat');
                }}
              >
                Cat
              </Radio>
              <Radio
                value="clear"
                size="lg"
                onChange={(e) => {
                  fetchGetAll(e);
                }}
              >
                Clear filter
              </Radio>
            </Stack>
          </RadioGroup>
          <TableContainer sx={{ margin: '2rem' }}>
            <Table variant="simple">
              <Thead>
                <Tr>
                  <Th fontSize="xl" fontFamily="body">
                    Pet
                  </Th>
                  <Th fontSize="xl" fontFamily="body">
                    Day
                  </Th>
                  <Th fontSize="xl" fontFamily="body">
                    Time
                  </Th>
                  <Th fontSize="xl" fontFamily="body">
                    Place
                  </Th>
                </Tr>
              </Thead>
              <Tbody>
                {history.map((element, index) => (
                  <Tr key={index}>
                    <Td>{element.pet}</Td>
                    <Td>{element.day}</Td>
                    <Td>{element.time}</Td>
                    <Td>{element.place}</Td>
                  </Tr>
                ))}
              </Tbody>
            </Table>
          </TableContainer>
          <br />
          <br />
        </GridItem>
      </Grid>
    </>
  );
}

export default History;
