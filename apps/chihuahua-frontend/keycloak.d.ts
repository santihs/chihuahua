declare module 'Keycloak' {
  interface KeyclaokToken {
    refresh_token: any;
    decoded: any;
    expires_at: number;
    realm_access: any;
    error: any;
    access_token: string;
    id_token: string;
    expires_at: string;
    refresh_token: string;
  }
}
