'use client';

import { Card, LineChart } from '@tremor/react';
import { addDays, format } from 'date-fns';
import axios from 'axios';

const PATH = process.env.BACKEND;
const place = 'Melchor Perez Avenue and Simon Lopez Avenue';

export type DailyPerformance = {
  date: string;
  Dog: number;
  Cat: number;
};

async function load(): Promise<DailyPerformance[]> {
  const currentDate = new Date();
  const startDate = new Date(
    currentDate.getFullYear(),
    currentDate.getMonth(),
    currentDate.getDate(),
  );
  const numberOfDays = 30;
  const placeFormat = encodeURIComponent(place);
  const performance: DailyPerformance[] = [];

  for (let i = 0; i < numberOfDays; i++) {
    const currentDate = addDays(startDate, -i);
    const formattedDate: string = format(currentDate, 'dd-MM-yyyy');
    const responseCat = await axios.get(
      `${PATH ?? 'http://localhost:5001'}/history/pet/cat/` +
        placeFormat +
        `/` +
        formattedDate,
    );
    const responseDog = await axios.get(
      `${PATH ?? 'http://localhost:5001'}/history/pet/dog/` +
        placeFormat +
        `/` +
        formattedDate,
    );
    const totalCat: number = responseCat.data.length;
    const totalDog: number = responseDog.data.length;
    performance.unshift({
      date: formattedDate,
      Dog: totalDog,
      Cat: totalCat,
    });
  }
  return performance;
}

export const ChartView = async () => {
  const filter = 'Time line in ' + place;
  const chartdata3 = await load();
  return (
    <Card className="max-w-lg mx-auto p-0">
      <h1>{filter}</h1>
      <div className="p-6">
        <LineChart
          className="h-72 mt-4"
          data={chartdata3}
          index="date"
          categories={['Cat', 'Dog']}
          colors={['neutral', 'indigo', 'rose', 'orange', 'emerald']}
          yAxisWidth={30}
        />
      </div>
    </Card>
  );
};

export default ChartView;
