'use client';

import { extendTheme } from '@chakra-ui/react';
import '@fontsource/irish-grover';
import '@fontsource-variable/inter';

export const myNewTheme = extendTheme({
  colors: {
    primary: '#6B7A8F',
    secondary: '#F7882F',
    warning: '#ffc75f',
    black: '#000000',
  },
  fonts: {
    body: 'Inter Variable',
    heading: 'Irish Grover',
  },
});
