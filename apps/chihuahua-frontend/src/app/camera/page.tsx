'use client';

import CustomWebcam from '@/components/CustomWebcam';
import SimpleSidebar from '@/components/SideBar';
import { useCameraStore } from '../../store/cameraStore';
import {
  Center,
  FormControl,
  FormLabel,
  Grid,
  GridItem,
  Heading,
  Switch,
} from '@chakra-ui/react';
import { useSession } from 'next-auth/react';
import axios from 'axios';
const PATH = process.env.BACKEND;

export default function Camera() {
  const { data: session, status } = useSession({
    required: true,
  });

  const { changePermit } = useCameraStore();

  const { isPermit } = useCameraStore((state) => ({
    isPermit: state.isPermit,
  }));

  const send = async () => {
    const dataDeactivation = {
      to: session?.user?.email,
      subject: 'Successful Camera Deactivation in Pet-Patrol',
      html: 'Hello, <br/>   <br/>  I hope this message finds you well. I am a member of the technical support team at Pet-Patrol. I am writing to confirm that you have successfully deactivated the camera in our application.\n\nWe want to ensure that the deactivation process went as expected and that you can now enjoy the application without the use of the camera. We are pleased to know that you find our customization and privacy options helpful. <br/>  <br/>  Thank you for trusting Pet-Patrol.  <br/>  <br/> Best regards, <br/>   <br/>  Pet-Patrol',
    };
    const dataActivation = {
      to: session?.user?.email,
      subject: 'Successful Activation of the Camera in Pet-Patrol',
      html: 'Hello, <br/>  <br/> I hope you are well. I am from the technical support team at Pet-Patrol. I am writing to confirm that you have successfully activated the camera in our application. <br/> We wanted to ensure that the activation process went as expected and that you can now enjoy the application with the use of the camera. We are pleased to know that you find our customization and privacy options helpful. <br/>  <br/>  Thank you for trusting Pet-Patrol. <br/> Regards, <br/>  <br/>  Pet-Patrol',
    };
    const data = isPermit ? dataDeactivation : dataActivation;
    await axios.post(`${PATH ?? 'http://localhost:5001'}/sendEmail`, data);
  };

  const handleChange = () => {
    changePermit(isPermit);
    send();
  };

  if (status === 'loading') {
    return <p>Loading or not authenticated</p>;
  }

  return (
    <>
      <Grid
        templateAreas={`
                  "nav main"
                  `}
        gridTemplateRows={'50px 1fr 30px'}
        gridTemplateColumns={'150px 1fr'}
        gap="1"
      >
        <GridItem area={'nav'}>
          <SimpleSidebar />
        </GridItem>
        <GridItem ml="200" area={'main'}>
          <Heading className="text-center" fontSize="6xl" mt="40px" mb="30px">
            Camera
          </Heading>
          <FormControl display="flex" alignItems="center" size="lg" mb="20px">
            <FormLabel htmlFor="email-alerts" mb="0" fontSize="2xl">
              Disable camera
            </FormLabel>
            <Switch isChecked={isPermit} onChange={handleChange} />
          </FormControl>
          <Center>{isPermit && <CustomWebcam displayStyle="inline" />}</Center>
        </GridItem>
      </Grid>
    </>
  );
}
