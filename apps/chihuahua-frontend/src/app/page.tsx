'use client';

import SimpleSidebar from '@/components/SideBar';
import { Center, Grid, GridItem, Heading, Image, Text } from '@chakra-ui/react';

export default function Home() {
  return (
    <>
      <Grid
        templateAreas={`
                  "nav main"
                  `}
        gridTemplateRows={'50px 1fr 30px'}
        gridTemplateColumns={'150px 1fr'}
        gap="1"
      >
        <GridItem area={'nav'}>
          <SimpleSidebar />
        </GridItem>
        <GridItem ml="200" mt="10" area={'main'} mr="45">
          <Heading className="text-center" fontSize="6xl" mt="10px">
            Pet Patrol
          </Heading>
          <Text fontSize="xl" mt="20px" mb="10">
            Analysis system app designed to help entrepreneurs and pet
            businesses make decisions about the best locations for their
            services. By leveraging advanced object detection technology and a
            network of cameras strategically placed throughout the city.
            <br /> It continuously monitors and analyzes the movement of pets in
            various areas. The system detects and tracks in real-time, providing
            valuable insights into high-traffic. With detailed reports and event
            notifications.
            <br /> PetPatrol empowers veterinarians, groomers, and other
            pet-focused business to optimize their business strategies and
            provide the best services.
          </Text>
          <Center>
            <Image src="logo-petpatrol.png" alt="Logo" htmlWidth="400px" />
          </Center>
        </GridItem>
      </Grid>
    </>
  );
}
