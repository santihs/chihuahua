import React, { useEffect, useState } from 'react';

function App() {
  const [message, setMessage] = useState('');
  const [receivedMessages, setReceivedMessages] = useState<string[]>([]);

  useEffect(() => {
    const ws = new WebSocket('ws://localhost:3001');

    ws.onmessage = (event) => {
      console.log(`Message received through WebSocket : ${event.data}`);
      setReceivedMessages((prevMessages) => [...prevMessages, event.data]);
    };

    return () => {
      ws.close();
    };
  }, []);

  const sendMessage = async () => {
    await fetch('http://localhost:3001/send', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ message }),
    });
    setMessage('');
  };

  return (
    <div>
      <h1>
        Rabbit Mq Poc
      </h1>
      <input
        type="text"
        value={message}
        onChange={(e) => setMessage(e.target.value)}
        placeholder="Write a message"
      />
      <button onClick={sendMessage}>Enviar Mensaje</button>

      <div style={{ marginTop: '20px' }}>
        <h2>Messages :</h2>
        <ul style={{ maxHeight: '200px', overflowY: 'scroll', listStyleType: 'none', padding: '0' }}>
          {receivedMessages.map((msg, index) => (
            <li key={index} style={{ background: '#ffffff', margin: '5px 0', padding: '10px', borderRadius: '5px' }}>
              {msg}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default App;
