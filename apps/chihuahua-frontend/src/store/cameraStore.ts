import create from 'zustand';

interface CameraState {
  isPermit: boolean;
  changePermit: (value: boolean) => void;
}

export const useCameraStore = create<CameraState>((set) => ({
  isPermit: true,
  changePermit: (value: boolean) => {
    set((state) => ({
      isPermit: !state.isPermit,
    }));
  },
}));
