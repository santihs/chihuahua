## CI configuration

Automate tests, code compilation and code building is important in software development, it guarantees the software qualitty avoiding and correcting errors early

This POC show a variant of the frontend and backend projects the `.gitlab-ci.yml` is the configuration file in gitlab that executes tasks automatically when exists changes in the repository

To manage a monorepo file is possible to stablish the tasks by project allowing us to manage separately each project. In both backend and frontend projects the jobs will be executed separately

### .backend-gitlab-ci.yml

```yaml
image: node:latest

cache:
  paths:
    - node_modules/

build-backend:
  stage: build
  script:
    - cd pocs/ci_frontend_backend/backend-poc
    - npm install
    - npm run build
```

### .frontend-gitlab-ci.yml

```yaml
image: node:latest

cache:
  paths:
    - node_modules/

build-frontend:
  stage: build
  script:
    - cd pocs/ci_frontend_backend/frontend-poc
    - npm install
    - npm run build
  artifacts:
    paths:
      - pocs/ci_frontend_backend/frontend-poc/.next
```
