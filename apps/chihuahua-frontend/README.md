This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

## Tremor library

### What is Tremor?

The React library to build dashboards fast. Modular components to build dashboards in a breeze.

### Install

Install Tremor from your command line via npm.

```sh
npm install @tremor/react
```

Add the path to the Tremor module to your `tailwind.config.js`.

```typescript
/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    "./src/**/*.{js,ts,jsx,tsx}",
    "./node_modules/@tremor/**/*.{js,ts,jsx,tsx}", // Tremor module
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
```

### Fonts 

For the fonts we are using for the letters in the project they are:
- Irish Grover (for titles)
- Inter (for the body)
Its dependencies are:
  `"@fontsource-variable/inter": "^5.0.15"`
  `"@fontsource/irish-grover": "^5.0.8"`

We obtained them from the page:  `https://fontsource.org/`


install dependencies with 
```
- npm install @fontsource-variable/inter
- npm install @fontsource/irish-grover
- npm install
```
The icons are from chakra ui, install dependencies with:
```
- npm i @chakra-ui/icons
- npm i
```
