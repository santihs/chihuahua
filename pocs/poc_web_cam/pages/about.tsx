import Link from 'next/link';
import Layout from '../components/Layout';
import CustomWebcam from '../components/CustomWebcam';
import { Heading, Image, Text } from '@chakra-ui/react';

const AboutPage = () => (
  <Layout title="About | Next.js + TypeScript Example">
    <Heading fontSize="5xl">About</Heading>
    <Text fontSize="sm">This is the about page</Text>
    <Text fontSize="sm" color="blue">
      <Link href="/">Go home</Link>
    </Text>
    <Image
      src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Cuesta_del_obispo_01.jpg/800px-Cuesta_del_obispo_01.jpg"
      alt="image"
      borderRadius="lg"
    />
    <CustomWebcam show={'none'} />
  </Layout>
);

export default AboutPage;
