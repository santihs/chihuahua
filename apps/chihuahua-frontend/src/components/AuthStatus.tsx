'use client';

import { useSession, signIn, signOut } from 'next-auth/react';
import { useEffect } from 'react';
import { AuthStatusButton } from './AuthStatusButton';
import { Spinner, Box } from '@chakra-ui/react';

async function keycloakSessionLogOut() {
  try {
    await fetch(`/api/auth/logout`, { method: 'GET' });
  } catch (err) {
    console.error(err);
  }
}

export default function AuthStatus() {
  const { data: session, status } = useSession();

  useEffect(() => {
    if (
      status !== 'loading' &&
      session &&
      session?.expires === 'RefreshAccessTokenError'
    ) {
      signOut({ callbackUrl: '/' });
    }
  }, [session, status]);

  if (status === 'loading') {
    return (
      <Box p={5}>
        <Spinner color="orange" />
      </Box>
    );
  } else if (session) {
    return (
      <div className="my-3">
        <AuthStatusButton
          label={'Log out'}
          onClickButton={() => {
            keycloakSessionLogOut().then(async () => {
              await signOut({ callbackUrl: '/' });
            });
          }}
        />
      </div>
    );
  }

  return (
    <div className="my-3">
      <AuthStatusButton
        label={'Log in'}
        onClickButton={() => {
          signIn('keycloak');
        }}
      />
    </div>
  );
}
