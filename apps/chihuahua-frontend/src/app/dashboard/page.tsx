'use client';

import SimpleSidebar from '@/components/SideBar';
import { Button, Grid, GridItem, Heading, Stack } from '@chakra-ui/react';
import { Dashboard } from './dashboard';
import { TriangleDownIcon } from '@chakra-ui/icons';
import { useSession } from 'next-auth/react';

export default function Profile() {
  const { status } = useSession({
    required: true,
  });

  if (status === 'loading') {
    return 'Loading or not authenticated...';
  }

  return (
    <>
      <Grid
        templateAreas={`
                  "nav main"`}
        gridTemplateRows={'50px 1fr 30px'}
        gridTemplateColumns={'150px 1fr'}
        gap="1"
        fontWeight="bold"
      >
        <GridItem pl="0" area={'nav'}>
          <SimpleSidebar />
        </GridItem>
        <GridItem ml="200" area={'main'} mr="45">
          <Heading className="text-center" fontSize="6xl" mt="40px">
            Dashboard
          </Heading>
          <Stack direction="row" spacing={1}>
            <Button leftIcon={<TriangleDownIcon />} variant="solid" isDisabled>
              Download
            </Button>
          </Stack>
          <Dashboard />
        </GridItem>
      </Grid>
    </>
  );
}
