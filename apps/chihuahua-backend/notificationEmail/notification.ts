import { Resend } from 'resend';

import dotenv from 'dotenv';
dotenv.config();

const Vfrom = 'Pet-Patrol <onboarding@resend.dev>';

export async function sendEmail(Vto: string, Vsubject: string, Vhtml: string) {
  const resend = new Resend(process.env.RESEND_API_KEY as string);

  try {
    const data = await resend.emails.send({
      from: Vfrom,
      to: [process.env.OWNER_GMAIL as string, Vto],
      subject: Vsubject,
      html: Vhtml,
    });
    return data.data;
  } catch (error) {
    console.error(error);
  }
}
