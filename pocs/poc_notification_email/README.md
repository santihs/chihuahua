# Resend with Node.js

This example shows how to use Resend with [Node.js](https://nodejs.org).

## Prerequisites

To get the most out of this guide, you’ll need to:

* [Create an API key](https://resend.com/api-keys)
* [Verify your domain](https://resend.com/domains)

## Instructions

1. Replace `re_123456789` on `.env` with your API key.
2. Replace `example@gmail.com` on `notification.ts` with the destination email.

3. Install dependencies:

  ```sh
npm install
  ```

4. Execute the following command:

  ```sh
npm run devts
  ```

## License

MIT License