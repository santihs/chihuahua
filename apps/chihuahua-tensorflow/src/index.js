import fastify from 'fastify';
import cors from '@fastify/cors';
import fastifyIO from 'fastify-socket.io';
import { transform } from './utils/transform.js';
import { analyze } from './utils/detector.js';

import '@tensorflow/tfjs-node';

const app = fastify();

app.register(cors, {
  origin: '*',
});

app.register(fastifyIO, {
  cors: {
    origin: '*',
  },
});

app.ready((err) => {
  if (err) throw err;

  app.io.on('connection', (socket) => {
    console.info('connected: ', socket.id);

    socket.on('message', async (data) => {
      const buffer = transform(data);
      await analyze(buffer);
    });
  });
});

app.listen({ port: 9090 }, () => {
  console.info(`Server running on http://localhost:9090`);
});
