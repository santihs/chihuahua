import { Metadata } from 'next';
import { Inter } from 'next/font/google';
import './globals.css';
import { ChakraProvider } from '@chakra-ui/react';
import { myNewTheme } from '@/styles/theme';
import SessionProviderWrapper from '@/utils/sessionProviderWraper';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'PetPatrol',
  description: 'Pets control',
  icons: {
    icon: 'logo-tab.png',
  },
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <SessionProviderWrapper>
          <ChakraProvider resetCSS theme={myNewTheme}>
            {children}
          </ChakraProvider>
        </SessionProviderWrapper>
      </body>
    </html>
  );
}
