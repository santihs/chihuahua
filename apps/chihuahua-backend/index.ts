import Fastify from 'fastify';
import cors from '@fastify/cors';
import historyRoutes from './routes/history';
import mongoConnector from './utils/dbConnector';
import { RabbitMQClient } from './rabbit-mq/RabbitMQClient';
import * as dotenv from 'dotenv';

dotenv.config();

const server = Fastify();

const rabbitMQUrl =
  process.env.CLOUDAMQP_URL ||
  'amqps://acaumfcb:WQg94XetSnqvGzF_RHmHf1m6cptVfzCh@fly.rmq.cloudamqp.com/acaumfcb';
const rabbitMQClient = new RabbitMQClient(rabbitMQUrl, 5);

declare module 'fastify' {
  export interface FastifyInstance {
    rabbitMQ: RabbitMQClient;
  }
}

const startServer = async () => {
  try {
    await rabbitMQClient.connect();
    server.decorate('rabbitMQ', rabbitMQClient);

    server.register(cors, {});
    server.register(mongoConnector);
    server.register(historyRoutes);

    const PORT: number = parseInt(process.env.FASTIFY_PORT || '5001');
    server.listen({ port: PORT }, (err, address) => {
      if (err) {
        console.error(err);
        process.exit(1);
      }
      console.log(`Server listening at ${address}`);
    });

    server.addHook('onClose', async (server) => {
      await server.rabbitMQ.close();
    });
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

startServer();
