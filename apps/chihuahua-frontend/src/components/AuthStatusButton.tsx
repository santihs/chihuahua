'use client';

import { Button } from '@chakra-ui/react';
import { ArrowBackIcon } from '@chakra-ui/icons';
import React from 'react';

interface AuthStatus {
  label: string;
  onClickButton?: () => void;
}

export const AuthStatusButton: React.FC<AuthStatus> = ({
  label,
  onClickButton,
}) => {
  return (
    <Button
      justifyContent="flex-start"
      type="button"
      colorScheme="white"
      _hover={{ bg: 'secondary', textColor: 'white' }}
      size="lg"
      height="60px"
      width="300px"
      textColor="black"
      fontSize="2xl"
      leftIcon={<ArrowBackIcon />}
      iconSpacing="4"
      onClick={() => {
        onClickButton && onClickButton();
      }}
    >
      {label}
    </Button>
  );
};
