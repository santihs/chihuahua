import { node } from '@tensorflow/tfjs-node';
import { load } from '@tensorflow-models/coco-ssd';
import sharp from 'sharp';
import date from 'date-and-time';
import axios from 'axios';

const PATH = process.env.BACKEND;

export const analyze = async (buffer) => {
  const model = await load();

  const resized = await sharp(buffer).resize(300, 300).toBuffer();
  const imageTensor = node.decodeImage(resized, 3);

  const predictions = await model.detect(imageTensor);
  predictions.forEach(async (prediction) => {
    if (
      prediction.class === 'dog' ||
      prediction.class === 'cat' ||
      prediction.class === 'bird'
    ) {
      const currentDate = new Date();

      const data = {
        pet: prediction.class,
        score: prediction.score,
        day: date.format(currentDate, 'DD-MM-YYYY'),
        time: date.format(currentDate, 'HH:mm:ss'),
        place: 'Melchor Perez Avenue and Simon Lopez Avenue',
      };

      await axios.post(`${PATH ?? 'http://localhost:5001'}/history`, data);
    }
  });
};
