# Backend

## Fastify

Only `index.ts` can be modified, If `index.ts` is modified you should run first

```sh
npm run build
```

and then

```sh
npm run dev
```

This will compile `index.ts` into `index.js` which can be executed using Node.js.

## Folders

- Routes: handle and organize all the different routes(endpoints) of the API.
- Controllers: folder created to separate our routes from our schemas and handlers
  - Handlers: This file will contain all our handler functions for our post routes (getting all posts, creating a post, deleting a post, etc.).
  - Schemas: This file will contain all the schemas for our post routes (getting all posts, creating a post, deleting a post, etc.).

## Properties

The three properties of the object of options we will be using in this API are

- schema: defines how our data should be set up, what data should come in, and what data should go out, including their types (string, boolean, number, etc).
- preHandler: a function that defines what should be done before requests are handled by the handler below.
- handler: a function that handles the request.

## Files

- cloud/posts.js: where the array of posts is stored (our database).
- routes/posts.js: where we handle all routes of our blog posts.
- handlers/posts.js: where we handle responses to our post routes.
- schemas/posts.js: where we specify the schemas of our post routes.

## Useful links:

- [Fastify with Typecript](https://fastify.dev/docs/latest/Reference/TypeScript#typescript)
- [CRUD with Fastify](https://dev.to/elijahtrillionz/build-a-crud-api-with-fastify-688)

- routes folder handle and organize all the different routes(endpoints) of the API.

## Eslint 
- Eslint: is a code analyzer and looks for errors based on the rules that are provided, such as syntax, if we have an unused variable we can have a rule that marks it as an error or warning  
- Prettier: Is a code formatter that helps us to have a defined style to have more uniform code, such as line breaks, spaces o tabs, semicolons in end line and other rules.

## Comands

This command initializes a new npm (Node Package Manager) package and sets up ESLint with a predefined configuration provided by the @eslint/config package.
```
npm init @eslint/config
```
Initializes an ESLint configuration file
```
npx eslint --init
```
This command installs two packages (eslint-plugin-prettier and eslint-config-prettier) as development dependencies (--save-dev). This command installs two packages (eslint-plugin-prettier and eslint-config-prettier) as development dependencies (--save-dev). These packages are used to integrate Prettier with ESLint.
```
npm install eslint-plugin-prettier eslint-config-prettier --save-dev
```
Used to execute ESLint
``` 
npm run lint
```
Used to execute Prettier
```
npm run format
```
