import { DetectedObject } from '@tensorflow-models/coco-ssd';

interface DrawOptions {
  className: string;
  probability: number;
}

export const drawFoundedObject = (
  detections: DetectedObject[],
  ctx: CanvasRenderingContext2D,
) => {
  detections.forEach((prediction) => {
    const [x, y, width, heigth] = prediction.bbox;
    const text = prediction.class;

    if (text === 'cat' || text === 'dog') {
      const color = Math.floor(Math.random() * 16777215).toString(16);
      ctx.strokeStyle = '#' + color;
      ctx.font = '18px Arial';
      ctx.fillStyle = '#' + color;

      ctx.beginPath();
      ctx.fillText(text, x, y);
      ctx.rect(x, y, width, heigth);
      ctx.stroke();
    }
  });
};

export const drawWithProbability = (
  detections: DrawOptions[],
  ctx: CanvasRenderingContext2D,
) => {
  detections.forEach((detection) => {
    const { className } = detection;
    const color = Math.floor(Math.random() * 16777215).toString(16);
    ctx.strokeStyle = '#' + color;
    ctx.font = '18px Arial';
    ctx.fillStyle = '#' + color;

    ctx.beginPath();
    ctx.fillText(`${className}`, 10, 20);
    ctx.stroke();
  });
};
