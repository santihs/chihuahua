import axios from 'axios';

const apiBaseUrl = 'http://localhost:5001';

const axiosClient = axios.create({
  baseURL: apiBaseUrl,
});

export async function getAllStatus() {
  try {
    const response = await axiosClient.get('/history');
    return response.data;
  } catch (error) {
    console.error('Error all status:', error);
  }
}

export async function getAllPets(pet: string) {
  try {
    const response = await axiosClient.get(`/history/pet/${pet}`);
    return response.data;
  } catch (error) {
    console.error('Error all status:', error);
  }
}

export const historyApi = {
  getAllStatus,
  getAllPets,
};
