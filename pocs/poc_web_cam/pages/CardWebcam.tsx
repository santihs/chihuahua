import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Flex,
  Avatar,
  Box,
  Heading,
  Text,
  Button,
  ButtonGroup,
  Link,
} from '@chakra-ui/react';
import CustomWebcam from '../components/CustomWebcam';
import React from 'react';

function CardWebcam() {
  return (
    <>
      <Card maxW="md">
        <CardHeader>
          <Flex>
            <Flex flex="1" gap="4" alignItems="center" flexWrap="wrap">
              <Avatar name="Segun Adebayo" src="https://bit.ly/sage-adebayo" />
              <Box>
                <Heading size="sm">User 1</Heading>
                <Text>Park Lincon</Text>
              </Box>
            </Flex>
          </Flex>
        </CardHeader>
        <CardBody>
          <Text>Camera for User 1</Text>
        </CardBody>
        <CustomWebcam show={'display'} />
        <CardFooter>
          <ButtonGroup spacing="2">
            <Button variant="solid" colorScheme="blue">
              <Link href="/CardItem">Close</Link>
            </Button>
          </ButtonGroup>
        </CardFooter>
      </Card>
    </>
  );
}

export default CardWebcam;
