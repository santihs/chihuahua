'use client';
// ... (import statements remain unchanged)

import { Button } from '@chakra-ui/react';
import { useRouter } from 'next/navigation';
import React from 'react';

interface SidebarButtonProps {
  icon: React.ElementType;
  label: string;
  route: string;
}

export const SidebarButton: React.FC<SidebarButtonProps> = ({
  icon,
  label,
  route,
}) => {
  const router = useRouter();

  return (
    <Button
      justifyContent="flex-start"
      type="button"
      colorScheme="white"
      _hover={{ bg: 'secondary', textColor: 'white' }}
      size="lg"
      height="55px"
      width="300px"
      textColor="black"
      fontSize="2xl"
      onClick={() => {
        router.push(route);
      }}
    >
      {icon && React.createElement(icon, { mr: '4' })}
      {label}
    </Button>
  );
};
