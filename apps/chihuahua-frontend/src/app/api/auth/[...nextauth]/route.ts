import NextAuth from 'next-auth';
import { authOptions } from '@/utils/authOptions';

// @ts-expect-error: types managed on authOptions no are available on AuthNext.js documentation
const handler = NextAuth(authOptions);

export { handler as GET, handler as POST };
