# PetPatrol - Chihuahuas

## Description

Analysis system app designed to help entrepreneurs and pet businesses make decisions about the best locations for their services. By leveraging advanced object detection technology and a network of cameras strategically placed throughout the city. It continuously monitors and analyzes the movement of pets in various areas. The system detects and tracks in real-time, providing valuable insights into high-traffic. With detailed reports and event notifications. PetPatrol empowers veterinarians, groomers, and other pet-focused business to optimize their business strategies and provide the best services.

## Structure

### Monorepo structure

For the monorepo structure we are using [Turborepo](https://turbo.build/) in order to run codebase's tasks like `lint`, `build` an `test` as fast as they could.

## How to configure Keycloak

In order to configure Keycloak, you must first have the project's Docker running. Once you have the Docker running, you can follow these steps:

1. Access the Keycloak administration console at the following link: http://localhost:8080/admin/master/console/
2. Log in using the credentials configured in Docker.
3. Click on "master" in the upper-left corner and then click "Create Realm".
4. Copy the content of `keycloak-config/real_settings_v2/realm-chihuahua`.
5. After creating the realm, select it in the "Realms" section, where you will also have a realm called "master".
6. Access the newly created realm called "chihuahua", then go to:
    * Section "clients" -> "pet-patrol-client" -> "authorization" -> "settings" -> "import" -> "browse"
7. Select the file `auth-chihuahua` located in the same folder as `realm-chihuahua`, then click "Confirm".
8. Now, you need to use the `.env` file provided to the team. Copy this file to the `front-end` folder of the project.
9. In the "pet-patrol-client" section, generate a new Client secret by clicking "regenerate" in the "Credentials" section.
10. Copy the generated Client secret into the field "CLIENT_SECRET".

**Troubleshooting**

* If you are having trouble accessing the Keycloak administration console, make sure that the Docker container is running and that you are using the correct credentials.
* If you are having trouble creating a realm in Keycloak, make sure that the realm name is unique.
* If you are having trouble importing the authentication settings into the realm, make sure that the file `auth-chihuahua` is in the correct format.
* If you are having trouble generating a Client secret in Keycloak, make sure that you are using the correct credentials.

## For run the project

1. In root folder run `npm install`
2. Later `turbo run dev`
