import { ObjectId } from '@fastify/mongodb';
import { FastifyRequest, FastifyReply, FastifyPluginOptions } from 'fastify';
import { sendEmail } from '../notificationEmail/notification';
import opts, {
  FastifyRequestWithParams,
  FastifyRequestAdd,
  FastifyRequestQueryPet,
  FastifyRequestQueryPetDate,
  FastifyRequestSend,
} from '../controllers/schemas/history';
import { RabbitMQClient } from '../rabbit-mq/RabbitMQClient';

async function historyRoutes(fastify: FastifyPluginOptions) {
  const collection = fastify.mongo.db.collection('test1');
  const rabbitMQ: RabbitMQClient = fastify.rabbitMQ;

  fastify.get(
    '/history',
    async (request: FastifyRequest, reply: FastifyReply) => {
      const result = await collection.find().toArray();
      if (result.length === 0) {
        throw new Error('No documents found');
      }
      return await reply.send(result);
    },
  );

  fastify.get(
    '/history/:_id',
    async (request: FastifyRequestWithParams, reply: FastifyReply) => {
      const convertedId = new ObjectId(request.params._id);
      const result = await collection.findOne({ _id: convertedId });
      if (!result) {
        throw new Error('Invalid value');
      }
      return await reply.send(result);
    },
  );

  fastify.post(
    '/history',
    opts,
    async (request: FastifyRequestAdd, reply: FastifyReply) => {
      const result = await collection.insertOne({
        pet: request.body.pet,
        score: request.body.score,
        day: request.body.day,
        time: request.body.time,
        place: request.body.place,
      });

      await rabbitMQ.sendMessage(
        'historyQueue',
        `New history entry added: ${result.insertedId}`,
      );

      return await reply.send(result);
    },
  );

  fastify.get(
    '/history/pet/:pet',
    (request: FastifyRequestQueryPet, reply: FastifyReply) => {
      const result = collection
        .find({
          pet: request.params.pet,
        })
        .toArray();
      if (result.length === 0) {
        throw new Error('No documents found');
      }
      return result;
    },
  );

  fastify.get(
    '/history/pet/:pet/:place',
    (request: FastifyRequestQueryPet, reply: FastifyReply) => {
      const result = collection
        .find({
          pet: request.params.pet,
          place: request.params.place,
        })
        .toArray();
      if (result.length === 0) {
        throw new Error('No documents found');
      }
      return result;
    },
  );

  fastify.get(
    '/history/pet/:pet/:place/:day',
    (request: FastifyRequestQueryPetDate, reply: FastifyReply) => {
      const result = collection
        .find({
          pet: request.params.pet,
          day: request.params.day,
          place: request.params.place,
        })
        .toArray();
      if (result.length === 0) {
        throw new Error('No documents found');
      }
      return result;
    },
  );

  fastify.post(
    '/sendEmail',
    async (request: FastifyRequestSend, reply: FastifyReply) => {
      try {
        const result = await sendEmail(
          request.body.to,
          request.body.subject,
          request.body.html,
        );
        reply.send(result);
      } catch (error) {
        console.error(error);
        reply.status(500).send('Error sending the email ');
      }
    },
  );
}

export default historyRoutes;
