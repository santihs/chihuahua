# Docker Compose Configuration

This documentation covers the configuration of Docker Compose to deploy Keycloak and MongoDB services.

## Version

- Docker Compose version: `3.9`.

## Services

### Keycloak

Keycloak is configured as an authentication and identity management service.

- Docker image:** `quay.io/keycloak/keycloak:latest`.
- Port:** The service is exposed on port `8080`.
- **Environment Variables:**
    - `KEYCLOAK_USER: admin` (Administrator user)
    - `KEYCLOAK_PASSWORD: admin` (Administrator password)

### MongoDB

MongoDB is used as a NoSQL database.

- Docker image:** `mongo:latest`.
- Port:** The service is exposed on port `27017`.
- **Volumes:**
    - A volume named `mongo-data` is mounted on `/data/db` for data persistence.
- Environment Variables:** **MONGO_INITDB
    - MONGO_INITDB_ROOT_USERNAME: admin` (MongoDB root user)
    - MONGO_INITDB_ROOT_PASSWORD: admin` (root user password)
    - MONGO_INITDB_DATABASE: mydatabase` (Initial database name)
- **Automatic restart:** The service is automatically restarted (`restart: always`).

## Volumes

- `mongo-data`: Volume used to persist MongoDB data.

---

