import { ObjectId } from '@fastify/mongodb';

const opts = {
  schema: {
    body: {
      type: 'object',
      properties: {
        pet: { type: 'string' },
        score: { type: 'number' },
        day: {
          type: 'string',
          pattern: '^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$',
        },
        time: {
          type: 'string',
          pattern: '^([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$',
        },
        place: { type: 'string' },
      },
    },
  },
};

interface FastifyRequestWithParams {
  params: {
    _id: ObjectId;
  };
  response: {
    200: {
      type: 'object';
      properties: {
        _id: { type: 'string' };
        pet: { type: 'string' };
        score: { type: 'number' };
        day: { type: 'string' };
        time: { type: 'string' };
        place: { type: 'string' };
      };
    };
  };
}

interface FastifyRequestAdd {
  body: {
    type: 'object';
    required: ['pet', 'day', 'time', 'place'];
    pet: { type: 'string' };
    score: { type: 'number' };
    day: {
      type: 'string';
      pattern: '^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$';
    };
    time: {
      type: 'string';
      pattern: '^([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$';
    };
    place: { type: 'string' };
  };
  response: {
    200: {
      type: 'object';
      properties: {
        _id: { type: 'string' };
        pet: { type: 'string' };
        score: { type: 'number' };
        day: { type: 'string' };
        time: { type: 'string' };
        place: { type: 'string' };
      };
    };
  };
}

interface FastifyRequestSend {
  body: {
    to: string;
    subject: string;
    html: string;
  };
  response: {
    200: {
      type: 'object';
    };
  };
}

interface FastifyRequestQueryPet {
  params: {
    pet: string;
    place: string;
    date: string;
    time: string;
  };
  response: {
    200: {
      type: 'object';
      properties: {
        _id: { type: 'string' };
        pet: { type: 'string' };
        score: { type: 'number' };
        day: { type: 'string' };
        time: { type: 'string' };
        place: { type: 'string' };
      };
    };
  };
}

interface FastifyRequestQueryPetDate {
  params: {
    pet: string;
    place: string;
    day: string;
    time: string;
    timeInit: string;
    timeEnd: string;
  };
  response: {
    200: {
      type: 'object';
      properties: {
        _id: { type: 'string' };
        pet: { type: 'string' };
        score: { type: 'number' };
        day: { type: 'string' };
        time: { type: 'string' };
        place: { type: 'string' };
      };
    };
  };
}
export default opts;

export type {
  FastifyRequestWithParams,
  FastifyRequestAdd,
  FastifyRequestQueryPet,
  FastifyRequestQueryPetDate,
  FastifyRequestSend,
};
