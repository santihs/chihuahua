import express from 'express';
import amqp from 'amqplib';
import cors from 'cors';
import http from 'http';
import { Server as WebSocketServer } from 'ws';

const app = express();
app.use(cors({ origin: 'http://localhost:3000' }));
app.use(express.json());

const rabbitMqUrl = 'amqp://localhost';
const queue = 'test_queue';
const server = http.createServer(app);
const wss = new WebSocketServer({ server });

wss.on('connection', (ws) => {

  amqp.connect(rabbitMqUrl).then((connection) => {
    return connection.createChannel().then((channel) => {
      channel.assertQueue(queue, { durable: false });

      channel.consume(queue, (message) => {
        if (message) {
          const content = message.content.toString();
          console.log(`RabbitMQ Message received: ${content}`);
          ws.send(content);
        }
      }, { noAck: true });
    });
  }).catch(console.error);
});

app.post('/send', async (req, res) => {
  const message = req.body.message;
  try {
    const connection = await amqp.connect(rabbitMqUrl);
    const channel = await connection.createChannel();
    await channel.assertQueue(queue, { durable: false });
    channel.sendToQueue(queue, Buffer.from(message));
    res.status(200).send('Message sent');
  } catch (error) {
    console.error(error);
    res.status(500).send('Error sending the message');
  }
});

const port = 3001;
server.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
