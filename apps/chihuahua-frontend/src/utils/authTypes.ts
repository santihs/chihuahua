export interface Account {
  access_token: string;
  id_token: string;
  expires_at: number;
  refresh_token: string;
}

export interface Session {
  access_token: string;
  id_token: string;
  roles: string[];
  error?: string;
}
