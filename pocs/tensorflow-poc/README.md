# Project overview
The goal of this POC is to create a real time detection system using TensorflowSSD and Tensorflow.js and how how to implement in a react project 

## Installation and Setup
- Run `npm i` to install the dependenciesRun
- Run `npm run dev` to start the project

## Results and output
The results of the project show the detection of animals in real time using the webcam. The output is shown in the browser window in a square canvas that is drawn around the object detected. The output is shown in the image below.

![Animal detection](./Tensorflow.png) 

## Conclusion
The project shows how to implement a real time detection system using TensorflowSSD and Tensorflow.js and its use in a react project.

## Refereces
- [Object Detection (coco-ssd)](https://github.com/tensorflow/tfjs-models/tree/master/coco-ssd)
- [Object detection Tensorflow.js and react](https://youtu.be/uTdUUpfA83s?si=EEGnkdurgsDTht4x)
