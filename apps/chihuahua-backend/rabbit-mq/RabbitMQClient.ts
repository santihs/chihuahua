import { Connection, Channel, connect } from 'amqplib';

export class RabbitMQClient {
  private conn: Connection | null = null;
  private channel: Channel | null = null;
  private readonly maxRetries: number;

  constructor(
    private readonly url: string,
    maxRetries: number = 3,
  ) {
    this.maxRetries = maxRetries;
  }

  async connect(): Promise<void> {
    this.conn = await connect(this.url);
    this.channel = await this.conn.createChannel();
  }

  async sendMessage(queue: string, message: string): Promise<void> {
    await this.channel?.assertQueue(queue, { durable: false });

    for (let retryCount = 0; retryCount <= this.maxRetries; retryCount++) {
      try {
        this.channel?.sendToQueue(queue, Buffer.from(message));
        return;
      } catch (error) {
        console.error(
          `Error sending message (retry ${retryCount}/${this.maxRetries}):`,
          error,
        );
        if (retryCount < this.maxRetries) {
          await new Promise((resolve) =>
            setTimeout(resolve, 1000 * Math.pow(2, retryCount)),
          );
        } else {
          throw new Error(
            `Failed to send message after ${this.maxRetries} retries`,
          );
        }
      }
    }
  }

  async close(): Promise<void> {
    if (this.channel) {
      await this.channel.close();
    }
    if (this.conn) {
      await this.conn.close();
    }
  }
}
