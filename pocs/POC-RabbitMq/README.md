## POC with RabbitMQ, Node.js and React

## Description.

This project is a proof of concept (POC) demonstrating the integration of a RabbitMQ message queue into a web application developed with Node.js and React. The goal is to illustrate how RabbitMQ can facilitate asynchronous and efficient communication between the backend and frontend of an application.

## How it works

### General Architecture

- **Backend (Node.js/Express):** This component acts as an intermediary between RabbitMQ and the frontend. It is in charge of sending messages to RabbitMQ and receiving messages from it.
- **RabbitMQ:** It serves as a messaging system to facilitate asynchronous communication. It allows the backend to efficiently publish and consume messages.
- **Frontend (React):** A simple user interface that allows users to send messages to RabbitMQ through the backend and receive notifications of new messages.


### Message Flow
1. **Sending Messages:** Users send messages through the React interface, which in turn sends them to the Node.js server.
2. **Publishing to RabbitMQ:** The Node.js server receives the message and publishes it to a RabbitMQ queue.
3. **Message Consumption:** The server is also subscribed to the RabbitMQ queue and receives messages from it.
4. **Client Notification:** When a message is received from RabbitMQ, the server sends it to the client through a WebSocket connection.
5. **Message Display:** The React client receives and displays messages in real time.

## Configuration and Usage
### Prerequisites
- Node.js and npm installed.
- RabbitMQ running via Docker.
- Basic knowledge of JavaScript/TypeScript, React and Node.js.

### Run the POC
1. **Dependencies:**
    - Backend: `cd backend `
        - `npm install.`
        - `Docker compose up.`

    - Frontend: `cd frontend && npm install`.
2. **Run the Project:**
    - Start the Node.js server: `cd backend && npm start`
    - Start the React application: `cd frontend && npm start`.

## Benefits of Using RabbitMQ

- **Decoupling:** RabbitMQ allows the frontend and backend of an application to operate more independently, improving modularity.
- **Scalability:** Facilitates application scaling by efficiently handling large message volumes and traffic peaks.
- **Reliability:** RabbitMQ ensures that messages are not lost, even in case of processing failures.
- **Flexibility:** Allows different messaging patterns (such as queuing, publish/subscribe, etc.) to be implemented, making it suitable for a wide range of use cases.
- **Performance Enhancement:** By using asynchronous communication, RabbitMQ can increase the performance and efficiency of applications.

## Conclusion.

This POC demonstrates how integrating RabbitMQ into modern web applications can significantly improve the efficiency, scalability, and reliability of message handling between client and server.

