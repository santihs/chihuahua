import { getServerSession } from 'next-auth';
import { authOptions } from '@/utils/authOptions';
import { decrypt } from './encryption';

export async function getAccessToken() {
  // @ts-expect-error: types managed on authOptions no are available on AuthNext.js documentation
  const session = await getServerSession(authOptions);
  if (session !== null) {
    // @ts-expect-error: types managed on authOptions no are available on AuthNext.js documentation
    const accessTokenDecrypted = decrypt(session.access_token);
    return accessTokenDecrypted;
  }
  return null;
}

export async function getIdToken() {
  // @ts-expect-error: types managed on authOptions no are available on AuthNext.js documentation
  const session = await getServerSession(authOptions);
  if (session !== null) {
    // @ts-expect-error: types managed on authOptions no are available on AuthNext.js documentation
    const idTokenDecrypted = decrypt(session.id_token);
    return idTokenDecrypted;
  }
  return null;
}
