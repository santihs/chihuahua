import useFrameCapture from '@/hooks/useFrameCapture';
import Webcam from 'react-webcam';

interface CustomWebcamProps {
  displayStyle: React.CSSProperties['display'];
}

const CustomWebcam: React.FC<CustomWebcamProps> = ({ displayStyle }) => {
  const { webcamRef, canvasRef } = useFrameCapture();

  return (
    <>
      <Webcam
        ref={webcamRef}
        style={{
          display: displayStyle,
          position: 'relative',
          zIndex: 1,
        }}
      />

      <canvas
        ref={canvasRef}
        style={{
          display: 'none',
        }}
      ></canvas>
    </>
  );
};

export default CustomWebcam;
