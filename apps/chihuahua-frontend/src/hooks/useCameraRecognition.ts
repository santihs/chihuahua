import { drawFoundedObject } from '@/utils/draw';
import { ObjectDetection, load } from '@tensorflow-models/coco-ssd';
import { useEffect, useRef } from 'react';
import Webcam from 'react-webcam';

const useCameraRecognition = () => {
  const webcamRef = useRef<Webcam>(null);
  const canvasRef = useRef<HTMLCanvasElement>(null);

  const capture = async () => {
    const net = await load();
    setInterval(() => {
      detect(net);
    }, 10);
  };

  const detect = async (net: ObjectDetection) => {
    if (
      typeof webcamRef.current !== 'undefined' &&
      webcamRef.current !== null &&
      webcamRef.current?.video?.readyState === 4
    ) {
      const video = webcamRef.current.video;
      const videoWidth = webcamRef.current?.video.videoWidth;
      const videoHeight = webcamRef.current?.video.videoHeight;

      webcamRef.current.video.width = videoWidth;
      webcamRef.current.video.height = videoHeight;

      if (canvasRef.current !== null) {
        canvasRef.current.width = videoWidth;
        canvasRef.current.height = videoHeight;
      }

      const obj = await net.detect(video);

      const ctx = canvasRef.current?.getContext('2d');
      drawFoundedObject(obj, ctx!);
    }
  };

  useEffect(() => {
    capture();
  });

  return { useRef, webcamRef, canvasRef };
};

export default useCameraRecognition;
